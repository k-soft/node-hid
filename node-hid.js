//var HID = require('node-hid');
//var usbDetect = require('usb-detection');
const UsbHID = require('./usbhid.js');
const { v4: uuidv4 } = require('uuid');


//console.log('devices:', HID.devices());

class Userside
{
	constructor()
	{
		this.usbdevice_list = [];
	}
	add( instance )
	{

		for( let tag of this.usbdevice_list )
		{
			console.log( tag.vid + '===' + instance.vid, tag.pid + '==='+ instance.pid );
			if( tag.vid===instance.vid && tag.pid===instance.pid )
			{

				let item = HID.FindDevice( tag.vid, tag.pid );
				if( item !== null )
				{
					tag.action = item.action;
					tag.attached = item.attached;
					tag.vid = item.vid;
					tag.pid = item.pid;
					tag.device = item.device;
					tag.info = item.info;
				}
				else
				{
					tag.action = instance.action;
					tag.attached = instance.attached;
					tag.vid = instance.vid;
					tag.pid = instance.pid;
					tag.device = instance.device;
					tag.info = instance.info;
				}
				return;
			}
		}
		instance.uuid = uuidv4();

		let item = HID.FindDevice( instance.vid, instance.pid );
		if( item !== null )
		{
			instance.action = item.action;
			instance.attached = item.attached;
			instance.vid = item.vid;
			instance.pid = item.pid;
			instance.device = item.device;
			instance.info = item.info;
		}
		this.usbdevice_list.push(instance);
		console.log( this.usbdevice_list);
	}
	remove( instance )
	{
		for( let tag of this.usbdevice_list )
		{
			console.log( tag.vid + '===' + instance.vid, tag.pid + '==='+ instance.pid );
			if( tag.vid===instance.vid && tag.pid===instance.pid )
			{
				let index = this.usbdevice_list.indexOf(tag);
				this.usbdevice_list.splice( index, 1 );
				break;
			}
		}
		console.log( this.usbdevice_list);
	}
}

const HID = new UsbHID.UsbHID();
var userside = new Userside();


HID.init();
HID.AddDetectDevice( 0x04d8, 0x00dd, detect_device_cb );
HID.AddDetectDevice( 0x04d8, 0x00de, detect_device_cb );
console.log(HID.ScanUsbDeviceList.length);


function detect_device_cb( instance )
{
	console.log( 'detect_device_cb' );
	if( instance.action === 'attach' )
	{
		console.log( 'attach' );
		userside.add(instance);
//		HID.usbDetect.find(instance.vid, instance.pid, function(err, devices) { console.log('find', devices, err); });
	}
	if( instance.action === 'detach' )
	{
		console.log( 'detach' );
		userside.remove(instance);
	}
	if( instance.action === 'detect' )
	{
		console.log( 'detect' );
		userside.add(instance);
//		HID.usbDetect.find(instance.vid, instance.pid, function(err, devices) { console.log('find', devices, err); });
	}
}










//var devices = HID.devices();
//var deviceInfo = devices.find( function(d) {
//	var isTeensy = d.vendorId===0x04d8 && d.productId===0x00dd;
//
//	return isTeensy;// && d.usagePage===0xFFAB && d.usage===0x200;
//});



/*
setInterval(() => {
		
	usbDetect.find( 0x04d8,0x00dd, function(err,device){
		console.log( 'find device' );
		console.log( device,err );
	})

},100 );
*/
/*
function usbinit( vid,pid )
{
	let status = 0;

	usbDetect.find( vid,pid, function(err,device){
		if( err === undefined )
		{
			console.log( 'find device' );
			console.log( device );
			status = 1;
		}
		else
		{
			status = -1;
		}
		return status;
	}).then(status)
	{
		if( status == 1 )
		{
			const devices = HID.devices();
			const deviceInfo = devices.find( function(d) {
				let isTeensy = d.vendorId===0x04d8 && d.productId===0x00dd;
			
				return isTeensy;// && d.usagePage===0xFFAB && d.usage===0x200;
			});
			return deviceInfo			
		}
		if( status == 0 )
		{

		}
	}
}
*/



/*
class USBDevice
{
	constructor( usbMonitor, usbDriver )
	{
		this.usbDriver = usbDriver;
		this.usbMonitor = usbMonitor;
	}




	isExist( vid,pid,cb )
	{
		let status = 0;

		{
			if( status == 1 )
			{
				let device_list = this.usbDriver.devices()
				let DeviceInfo = device_list.find( function(d) {
					let isTeensy = d.vendorId===vid && d.productId===pid;
					return isTeensy;// && d.usagePage===0xFFAB && d.usage===0x200;
				});
				cb( DeviceInfo );
			}
			else
			{
				cb( null );
			}
		}
	}
	open( path )
	{
		let device = new this.usbDriver.HID( path );
		return device;
	}
	read()
	{
		let data = device.readSync();
		return data;
	}
	write( body )
	{
		let packet = body.slice();
//		for( let i = packet.length;i < length; i++ )
//		{
//			packet[i] = 0x00;
//		}
		packet.unshift(0x00);
		let size = device.write(packet);
		return size;
	}
}




UsbHID.prototype.find = async function ( vid, pid )
{
	await this.usbMonitor.find( vid, pid, function(err,device)
	{
		if( err === undefined )
		{
			return device;
		}
		else
		{
			return null;
		}
	})
}



*/











if( 0 )//deviceInfo )
{





	var device = new HID.HID( deviceInfo.path );
	console.log( device );

/*
	device.gotData = function (err, data) {
		console.log('got ps3 data', data);
		this.read(this.gotData.bind(this));
	};
	
	device.read(device.gotData.bind(device));
*/
/*
	device.on('data', (data)=>{
		console.log('got data', data);
	});
*/
	let HID_packet=[];
	let buf = [];
	let data = [];
	for( let i=0;i<66;i++ )
	{
		HID_packet[i]=0;
		data[i] = 0;
		buf[i] = 0;
	}
/*
	HID_packet[0] = 0xB0;
	HID_packet[1] = 0x00;
	device.write(HID_packet);
*/
/*
	HID_packet[0] = 0xB0;
	HID_packet[1] = 0x01;
	let size = device.write(HID_packet);
	console.log( 'trans:', size)
*/
//reset 0x00, 0x70, 0xAB, 0xCD, 0xEF
/*
	HID_packet[0] = 0x00;
	HID_packet[1] = 0x70;
	HID_packet[2] = 0xAB;
	HID_packet[3] = 0xCD;
	HID_packet[4] = 0xEF;

	let size = device.write(HID_packet);
	console.log( 'trans:', size);
*/
//	data = device.readSync();
//	console.log('got data', data);



	const CONST_BASE = 1;
	const CONST_BASE_GPIO0 = CONST_BASE + 2;
	const CONST_BASE_GPIO1 = CONST_BASE + 6;
	const CONST_BASE_GPIO2 = CONST_BASE + 10;
	const CONST_BASE_GPIO3 = CONST_BASE + 14;


	for( let i=0;i<66;i++ )
	{
		HID_packet[i]=0;
		data[i] = 0;
		buf[i] = 0;
	}
	HID_packet.fill(0);
	data.fill(0);
	buf.fill(0);


	HID_packet[0] = 0x00;
	HID_packet[1] = 0x61;

	console.log( 'cmd 0x61 : trans:', HID_packet);
	size = device.write(HID_packet);

	data = device.readSync();
	console.log('cmd 0x61 : got data', data);


	buf[0] = 0x00;
	buf[CONST_BASE + 0] = 0x60;
	buf[CONST_BASE + 1] = 0x00;
	buf[CONST_BASE + 2] = 0x00;//data[5];
	buf[CONST_BASE + 3] = 0x00;//data[6];
	buf[CONST_BASE + 4] = 0x00;
	buf[CONST_BASE + 5] = 0x00;
	buf[CONST_BASE + 6] = 0x00;

	buf[CONST_BASE + 7] = 0x80;
	buf[CONST_BASE + 8] = 0x00;
	buf[CONST_BASE + 9] = 0x00;
	buf[CONST_BASE + 10] = 0x00;
	buf[CONST_BASE + 11] = 0x00;

//	console.log( 'buf:', buf);

	console.log( 'cmd 0x60 : ', buf);
	size = device.write(buf);

	data = device.readSync();
	console.log('cmd 0x60 : got data', data);

	buf.fill(0);

	buf[0] = 0x00;
	buf[CONST_BASE + 0] = 0x50;
	buf[CONST_BASE + 1] = 0x00;

	buf[CONST_BASE_GPIO0 + 0] = 0xFF;//setting GPIO0 port value
	buf[CONST_BASE_GPIO0 + 1] = 0x01;//GPIO0 value = 0;
	buf[CONST_BASE_GPIO0 + 2] = 0xFF;//setting GPIO0 I/O mode
	buf[CONST_BASE_GPIO0 + 3] = 0x00;//GPIO0=output

	buf[CONST_BASE_GPIO1 + 0] = 0xFF;//setting GPIO1 port value
	buf[CONST_BASE_GPIO1 + 1] = 0x00;//GPIO1 value = 0;
	buf[CONST_BASE_GPIO1 + 2] = 0xFF;//setting GPIO1 I/O mode
	buf[CONST_BASE_GPIO1 + 3] = 0x00;//GPIO1=output

	buf[CONST_BASE_GPIO2 + 0] = 0xFF;//setting GPIO2 port value
	buf[CONST_BASE_GPIO2 + 1] = 0x01;//GPIO2 value = 0;
	buf[CONST_BASE_GPIO2 + 2] = 0xFF;//setting GPIO2 I/O mode
	buf[CONST_BASE_GPIO2 + 3] = 0x00;//GPIO2=output

	buf[CONST_BASE_GPIO3 + 0] = 0xFF;//setting GPIO3 port value
	buf[CONST_BASE_GPIO3 + 1] = 0x00;//GPIO3 value = 0;
	buf[CONST_BASE_GPIO3 + 2] = 0xFF;//setting GPIO3 I/O mode
	buf[CONST_BASE_GPIO3 + 3] = 0x00;//GPIO3=output

	console.log( 'cmd 0x50 : ', buf);
	size = device.write(buf);

	data = device.readSync();
	console.log('cmd 0x50 : got data', data);


	var out_value = 0x00;

	setInterval(() => {
		
		out_value = out_value === 0x00 ? 1 : 0x00;

		for( let i=0;i<66;i++ )
		{
			buf[i] = 0;
		}
	
		buf[0] = 0x00;
		buf[CONST_BASE + 0] = 0x50;
		buf[CONST_BASE + 1] = 0x00;
	
		buf[CONST_BASE_GPIO0 + 0] = 0xFF;//setting GPIO0 port value
		buf[CONST_BASE_GPIO0 + 1] = out_value;//GPIO0 value = 0;
		buf[CONST_BASE_GPIO0 + 2] = 0x00;//setting GPIO0 I/O mode
		buf[CONST_BASE_GPIO0 + 3] = 0x00;//GPIO0=output
	
		buf[CONST_BASE_GPIO1 + 0] = 0xFF;//setting GPIO1 port value
		buf[CONST_BASE_GPIO1 + 1] = out_value;//GPIO1 value = 0;
		buf[CONST_BASE_GPIO1 + 2] = 0x00;//setting GPIO1 I/O mode
		buf[CONST_BASE_GPIO1 + 3] = 0x00;//GPIO1=output
	
		buf[CONST_BASE_GPIO2 + 0] = 0xFF;//setting GPIO2 port value
		buf[CONST_BASE_GPIO2 + 1] = out_value;//GPIO2 value = 0;
		buf[CONST_BASE_GPIO2 + 2] = 0x00;//setting GPIO2 I/O mode
		buf[CONST_BASE_GPIO2 + 3] = 0x00;//GPIO2=output
	
		buf[CONST_BASE_GPIO3 + 0] = 0xFF;//setting GPIO3 port value
		buf[CONST_BASE_GPIO3 + 1] = out_value;//GPIO3 value = 0;
		buf[CONST_BASE_GPIO3 + 2] = 0x00;//setting GPIO3 I/O mode
		buf[CONST_BASE_GPIO3 + 3] = 0x00;//GPIO3=output
	
		console.log( 'cmd 0x50 : ', buf);
		size = device.write(buf);
	
		data = device.readSync();
		console.log('cmd 0x50 : got data', data);
	

	}, 1000);










// ... use device
}
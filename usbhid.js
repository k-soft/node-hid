var usb_hid = require('node-hid');
var Detector = require('usb-detection');

let UsbHID = function(){}

UsbHID.prototype.usbDetect = Detector;
UsbHID.prototype.HID = usb_hid;
UsbHID.prototype.ScanUsbDeviceList =[];// _ScanUsbDeviceList;


UsbHID.prototype.init = function()
{
    this.usbDetect.startMonitoring();

	console.log( 'init' );
	this.usbDetect.on('add', (device) =>{
		console.log( 'usbDetect.on.add' );
		
		if( UsbHID.prototype.ScanUsbDeviceList.length )
		{
			for( let tag of UsbHID.prototype.ScanUsbDeviceList )
			{
				if( tag.vid === device.vendorId && tag.pid === device.productId )
				{
					console.log( 'exist device' );
					tag.status = 1;
					tag.callback( 
					{
						action : 'attach',
						attached : 1,
						vid : tag.vid,
						pid : tag.pid,
						device : device,
						info : null,
					} );

					return;
				}
			}
		}
		console.log( 'no exist device' );
	});

	this.usbDetect.on('remove',(device)=>{
		console.log( 'usbDetect.on.remove' );
    
        if( UsbHID.prototype.ScanUsbDeviceList.length )
        {
            for( let tag of UsbHID.prototype.ScanUsbDeviceList )
            {
                if( tag.vid === device.vendorId && tag.pid === device.productId )
                {
					tag.status = 0;
					tag.callback( 
					{
                            action : 'detach',
							attached : 0,
                            vid : tag.vid,
                            pid : tag.pid,
							device : device,
							info : null,
					} );

					return;
                }
            }
        }
    })
};


UsbHID.prototype.FindDevice = function ( vid, pid )
{
	let list = UsbHID.prototype.HID.devices();
	list = [];
	list = UsbHID.prototype.HID.devices();
//	console.log('-----');
//	console.log(list);
//	console.log('-----');


	let item = list.find( function( d ){
		return (d.vendorId===vid && d.productId===pid);
	});
	
	if( item !== undefined )
	{
		console.log( 'find device' );
		let tag = {
			status : 2,
			action : 'find',
			attached : 1,
			vid : vid,
			pid : pid,
			device : null,
			info : item,
		}
		return tag;
	}
	return null;
}

UsbHID.prototype.AddDetectDevice = function ( vid, pid, callback )
{
    console.log( 'AddDetectDevice' );
//	console.log( this.ScanUsbDeviceList );
	if( this.ScanUsbDeviceList.length )
	{
		for( let tag of this.ScanUsbDeviceList )
		{
			if( tag.vid === vid && tag.pid === pid )
			{
                console.log( 'exist was device' );
				tag.callback = callback;
				return;
			}
		}
	}

	let tag = 
	{
		vid : vid,
		pid : pid,
		status : 0,
		callback : callback
	};
	this.ScanUsbDeviceList.push( tag );

	let item = this.FindDevice( vid, pid );
	if( item !== null )
	{
		item.action = 'detect';
		item.device = null;
		callback( item );
	}
	//    console.log( this.ScanUsbDeviceList );
}


UsbHID.prototype.open = function( path )
{
    let device = new HID.HID( path );
    return device;
}


UsbHID.prototype.read = function( device )
{
    let data = device.readSync();
    return data;
}


UsbHID.prototype.write = function( device, body )
{
    let packet = body.slice();
    packet.unshift(0x00);
    let size = device.write(packet);
    return size;
}












/*

var ScanUsbDeviceList=[];



usbDetect.startMonitoring();



usbDetect.on('add',function(device){
	console.log( 'add device' );
	console.log( device );

	if( ScanUsbDeviceList.length )
	{
        console.log( 'exist device' );
		for( let tag of ScanUsbDeviceList )
		{
            console.log( tag );
            console.log( tag.vid + '===' + device.vendorId, tag.pid + '==='+ device.productId );
			if( tag.vid === device.vendorId && tag.pid === device.productId )
			{
				tag.callback( 
					{
						action : 'attach',
						vid : tag.vid,
						pid : tag.pid,
						device : device
					 } );
				return;
			}
		}
	}
    console.log( 'no exist device' );
})

usbDetect.on('remove',function(device){
	console.log( 'remove device' );
	console.log( device );

	if( ScanUsbDeviceList.length )
	{
		for( let tag of ScanUsbDeviceList )
		{
			if( tag.vid === device.vendorId && tag.pid === device.productId )
			{
				tag.callback( 
					{
						action : 'detach',
						vid : tag.vid,
						pid : tag.pid,
						device : device
					 } );
				return;
			}
		}
	}
})





exports.AddDetectDevice = function ( vid, pid, callback )
{
	if( ScanUsbDeviceList.length )
	{
		for( let tag of ScanUsbDeviceList )
		{
			if( tag.vid === vid && tag.pid === pid )
			{
                console.log( 'exist device' );
				tag.callback = callback;
				return;
			}
		}
	}
	ScanUsbDeviceList.push( 
		{
			vid : vid,
			pid : pid,
			callback : callback
		}
	);
    console.log( 'add device' );
    console.log( ScanUsbDeviceList );

}



exports.open = function( path )
{
    let device = new HID.HID( path );
    return device;
}


exports.read = function( device )
{
    let data = device.readSync();
    return data;
}


exports.write = function( device, body )
{
    let packet = body.slice();
    packet.unshift(0x00);
    let size = device.write(packet);
    return size;
}

*/

exports.UsbHID = UsbHID;

